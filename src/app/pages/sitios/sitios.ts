import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList, LoadingController, ModalController, ToastController } from '@ionic/angular';

import { FiltroSitioPage } from '../sitio-filtro/sitio-filtro';
import { UserData } from '../../providers/user-data';
import { Services } from '../../providers/services';


@Component({
  selector: 'page-sitios',
  templateUrl: 'sitios.html',
  styleUrls: ['./sitios.scss'],
})
export class SitiosPage implements OnInit {
  @ViewChild('siteList', { static: true }) siteList: IonList;

  queryText = '';
  tabActiva = 'all';
  paisExcluido: any = [];
  mostrarSitio: any = [];
  sitios: any = [];
  paises: any = [];
  favoritos: any = [];
  load: any;
  userId: number;

  constructor(
    public alertCtrl: AlertController,
    public services: Services,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public toastCtrl: ToastController,
    public user: UserData
  ) { }

  async ionViewWillEnter() {
    this.load = await this.loadingCtrl.create({
      spinner: 'lines',
      showBackdrop: true,
      message: "Espere por favor...",
      translucent: true,
      cssClass: 'ion-loading',
      duration: 0
    });
    await this.load.present();
    this.user.isLoggedIn().then(m => {
      if (!m) {
        this.load.dismiss(this.loadingCtrl);
        return this.router.navigateByUrl('/login');
      } else {
        this.ngOnInit();
      }
    });
  }

  async ngOnInit() {
    this.traerInformacion();
  }

  async traerInformacion(callback = () => { }) {
    this.user.getUserId().then(id => {
      this.userId = id;
      this.services.getSites().subscribe((ListaSitios) => {
        this.services.getCountries().subscribe((ListaPaises) => {
          this.services.getFavsUser(this.userId).subscribe((ListaFavoritos) => {
            this.sitios = ListaSitios['sitio'];
            this.paises = ListaPaises;

            //this.paises.sort((a, b) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));

            this.favoritos.splice(0, this.favoritos.length);
            if (ListaFavoritos['favoritos']['codResultado'] == undefined) {
              ListaFavoritos['favoritos'].map(item => this.favoritos.push(item['id_sitio']));
            }
            this.user.setSites(this.sitios);
            this.verificarPais();
            this.actualizarSitios();
            callback();
            this.load.dismiss(this.loadingCtrl);
          });
        });
      });
    });
  }

  actualizarSitios() {
    if (this.siteList) {
      this.siteList.closeSlidingItems();
    }
    this.user.setFavorite(this.favoritos);
    this.mostrarSitio = 0;
    this.queryText = this.queryText.toLowerCase().replace(/,|\.|-/g, ' ');
    const queryWords = this.queryText.split(' ').filter(w => !!w.trim().length);
    this.paises.forEach((pais: any) => {
      pais.hide = true;
      this.sitios.forEach((sitio: any) => {
        if (pais.id_pais == sitio.id_pais) {
          this.filtrarSitio(sitio, queryWords, this.paisExcluido, this.tabActiva);
          if (!sitio.hide) {
            pais.hide = false && !pais.existe;
            this.mostrarSitio++;
          }
        }
      });
    });
    /*this.confData.getTimeline(this.dayIndex, this.queryText, this.paisExcluido, this.segment).subscribe((data: any) => {
      this.mostrarSitio = data.mostrarSitio;
      this.groups = data.groups;
    });*/
  }

  doRefresh(event) {
    this.traerInformacion(() => {
      event.target.complete();
    });
  }

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: FiltroSitioPage,
      componentProps: { excludedTracks: this.paisExcluido, countries: this.paises }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.paisExcluido = data;
      this.actualizarSitios();
    }
  }

  async agregarFavorito(slidingItem: HTMLIonItemSlidingElement, sitio: any) {
    if (this.user.hasFavorite(sitio.id_sitio)) {
      // woops, they already favorited it! What shall we do!?
      // prompt them to remove it
      this.quitarFavorito(slidingItem, sitio, 'Ya se ha agregado a la lista de favoritos');
    } else {
      // remember this session as a user favorite
      this.user.addFavorite(sitio.id_sitio);

      // create an alert instance
      const alert = await this.alertCtrl.create({
        header: 'Agregado a la lista de favoritos',
        buttons: [{
          text: 'OK',
          handler: () => {
            this.services.addfav({ "id_sitio": sitio.id_sitio, "id_usuario": this.userId }).subscribe((data) => {
              slidingItem.close();
            });
          }
        }]
      });
      // now present the alert on top of all other content
      await alert.present();
    }

  }

  async quitarFavorito(slidingItem: HTMLIonItemSlidingElement, sessionData: any, title: string) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: 'Quieres remover este sitio de tus favoritos?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            slidingItem.close();
          }
        },
        {
          text: 'Remover',
          handler: () => {
            this.services.removefav({ "id_sitio": sessionData.id_sitio, "id_usuario": this.userId }).subscribe((data) => {
              this.user.removeFavorite(sessionData.id_sitio);
              this.favoritos = this.user.getFavorite();
              this.actualizarSitios();
              slidingItem.close();
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async openSocial(network: string, fab: HTMLIonFabElement) {
    const loading = await this.loadingCtrl.create({
      message: `Posting to ${network}`,
      duration: (Math.random() * 1000) + 500
    });
    await loading.present();
    await loading.onWillDismiss();
    fab.close();
  }

  filtrarSitio(
    sitio: any,
    queryWords: string[],
    paisExcluido: any[],
    tabActiva: string
  ) {
    let matchesQueryText = false;
    if (queryWords.length) {
      queryWords.forEach((queryWord: string) => {
        if (sitio.nombre.toLowerCase().indexOf(queryWord) > -1) {
          matchesQueryText = true;
        }
      });
    } else {
      matchesQueryText = true;
    }

    let matchesTracks = false;

    if (!paisExcluido.includes(sitio.id_pais, 0)) {
      matchesTracks = true;
    }


    let matchesSegment = false;
    if (tabActiva === 'favorites') {
      if (this.user.hasFavorite(sitio.id_sitio)) {
        matchesSegment = true;
      }
    } else {
      matchesSegment = true;
    }
    sitio.hide = !(matchesQueryText && matchesTracks && matchesSegment);
  }

  verificarPais() {
    this.paises.map(pais => {
      this.sitios.map(sitio => {
        if (Number(sitio['id_pais']) == Number(pais['id_pais'])) {
          pais['existe'] = true;
        }
      })
    });
  }

}
