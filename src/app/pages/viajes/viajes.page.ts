import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViajeService } from 'src/app/services/viaje.service';
import { HttpClient } from '@angular/common/http';
//import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-viajes',
  templateUrl: './viajes.page.html',
  styleUrls: ['./viajes.page.scss'],
})
export class ViajesPage implements OnInit {

  results=null;

  constructor(private activatedRoute: ActivatedRoute,private viajeService: ViajeService, private http: HttpClient) { }

  idglob= '';
  nombreviaje = '';
  id: number;

  viajes = [];
  ngOnInit() {

    this.cargar();
    //this.fetchData();

  }

  cargar()
  {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.idglob = id;
    this.viajeService.getViajexUser(id).subscribe(result => {
      console.log('details',result);
      this.results = result;
    })

  }

  sendPostRequest()
  {
    this.http.post('https://easytrip-253600.appspot.com/viaje', {
      nombre: this.nombreviaje,
      id_usuario: this.idglob
    }).subscribe((response) => {
      console.log(response);
      this.cargar();
    })
  }

  fetchData = function(){
    this.http.get(`https://easytrip-253600.appspot.com/`).subscribe(
      (res: Response) => {
        this.viajes = res.json();
      })
  }

  sendDeleteRequestViaje(id){
    if(confirm("Esta Seguro de Eliminar")){
      return this.http.delete(`https://easytrip-253600.appspot.com/viaje/${id}`)
      .toPromise().then(()=>{
        this.fetchData();
        this.cargar();
      })
     // .subscribe((response) =>
      //{console.log(response);
      //})
    }
  }

}
