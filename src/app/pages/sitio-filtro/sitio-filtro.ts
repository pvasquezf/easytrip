import { AfterViewInit, Component } from '@angular/core';
import { Config, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-sitio-filtro',
  templateUrl: 'sitio-filtro.html',
  styleUrls: ['./sitio-filtro.scss'],
})
export class FiltroSitioPage implements AfterViewInit {
  ios: boolean;

  paises: {id:number, name: string, icon: string, isChecked: boolean, exists: boolean }[] = [];

  constructor(
    private config: Config,
    public modalCtrl: ModalController,
    public navParams: NavParams
  ) { }

  ionViewWillEnter() {
    this.ios = this.config.get('mode') === `ios`;
  }

  // TODO use the ionViewDidEnter event
  ngAfterViewInit() {
    // passed in array of track names that should be excluded (unchecked)
    const excludedTrackNames = this.navParams.get('excludedTracks');
    const countries = this.navParams.get('countries');

    countries.forEach(track => {
      this.paises.push({
        id: track.id_pais,
        name: track.pais,
        icon: "logo-angular",
        exists: track.existe==true?true:false,
        isChecked: (!excludedTrackNames.includes(track.id_pais, 0))
      });
    });
  }

  selectAll(check: boolean) {
    // set all to checked or unchecked
    this.paises.forEach(track => {
      track.isChecked = check;
    });
  }

  applyFilters() {
    // Pass back a new array of track names to exclude
    const excludedTrackNames = this.paises.filter(c => !c.isChecked).map(c => c.id);
    this.dismiss(excludedTrackNames);
  }

  dismiss(data?: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data);
  }
}
