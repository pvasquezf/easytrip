import { Component, OnInit } from '@angular/core';
import { ViajeportalService } from 'src/app/services/viajeportal.service'
import { HttpClient } from '@angular/common/http';
import { ViajeService } from 'src/app/services/viaje.service';
import { UserData } from '../../providers/user-data';
@Component({
  selector: 'app-portal-viaje',
  templateUrl: './portal-viaje.page.html',
  styleUrls: ['./portal-viaje.page.scss'],
})
export class PortalViajePage implements OnInit {
  
  idglob: number;
  nombreviaje = '';
  inputnombre = '';
  constructor(public viajesService: ViajeportalService, private http: HttpClient, public user: UserData) { 
  }

  async ngOnInit() {
    this.traerInformacion();
  }

  async traerInformacion(callback = () => { }) {
    this.user.getUserId().then(id => {
      this.idglob = id;    
      this.viajesService = new ViajeportalService(this.http);
      this.viajesService.id = this.idglob;

      this.viajesService.FetchViajes(id).then(result => result.json()).then(json => { 
        this.viajesService.viajes = json;
      })

      callback(); 
    });
  }

  sendPostRequest()
  {

    this.viajesService.PostViajes(this.idglob,this.nombreviaje).subscribe((response) => {
      console.log("POST RESULT",response);
      this.cargar();
    });
    
  }

  sendDeleteRequest(inputid)
  {
    this.viajesService.DeleteViajes(inputid).subscribe((response) => {
      console.log("DELETE RESULT",response);
      this.cargar();
    });
    
  }

  sendPutRequest(inputid, inputviaje)
  {
    this.viajesService.ActualizarViajes(inputid, this.inputnombre, inputviaje).subscribe((response) => {
      console.log("PuUT RESULT",response);
      this.cargar();
    });
    
  }


  cargar()
  {
    this.traerInformacion();
  }

}
