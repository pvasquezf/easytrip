import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { MenuController, LoadingController } from '@ionic/angular';
import { New_User } from '../../interfaces/user-options';
import { Services } from '../../providers/services';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  styleUrls: ['./signup.scss'],
})
export class SignupPage {
  signup: New_User = { nombre: '', apellido: '', email: '', pass: '', edad: 0 };
  submitted = false;
  load: any;
  constructor(
    public router: Router,
    public menu: MenuController,
    public userData: UserData,
    public loadingCtrl: LoadingController,
    public services: Services
  ) { }

  async ngOnInit() {
    this.load = await this.loadingCtrl.create({
      spinner: 'lines',
      showBackdrop: true,
      message: "Espere por favor...",
      translucent: true,
      cssClass: 'ion-loading',
      duration: 0
    });
    await this.load.present();
    this.userData.isLoggedIn().then(m => {
      this.load.dismiss(this.loadingCtrl);
      if (m) {
        this.menu.enable(true);
        return this.router.navigateByUrl('/app/tabs/sitios');
      }
      this.menu.enable(false);
    });
  }

  onSignup(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      let data = JSON.stringify(
        {
          "nombre": this.signup.nombre,
          "apellido": this.signup.apellido,
          "email": this.signup.email,
          "pass": this.signup.pass, 
          "edad": this.signup.edad
        });
      this.services.postUser(data).subscribe(json => {
          if (json['Salida'] == false) {
            this.router.navigateByUrl('/signup');
          } else {
            this.router.navigateByUrl('/login');
          }
        });
    } else {
      this.router.navigateByUrl('/signup');
    }
  }
}
