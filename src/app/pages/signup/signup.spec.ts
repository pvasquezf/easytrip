import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { SignupPage } from './signup';
import { ServicesTest } from 'src/app/providers/servicesTest';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

let servicios: ServicesTest;

describe('SignupPage', function () {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignupPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule, HttpClientTestingModule, IonicStorageModule.forRoot()],
      providers: [{ provide: Router }, ServicesTest]
    }).compileComponents();
    servicios = new ServicesTest();
  }));

  it('Debe crear la pagina de Signup', () => {
    const fixture = TestBed.createComponent(SignupPage);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('Debe retornar un valor valido al crear un usuario', function (done) {
    servicios.postUser("test1", "new_user", "test1@test.com", "test", 40).then(response => response.json())
      .then(json => {
        if (json['codResultado'] != undefined) {
          expect(json['Salida']).toBe(true);
        } else {
          expect(json['Salida']).toBe(false);
        }
        done();
      })
  });
});