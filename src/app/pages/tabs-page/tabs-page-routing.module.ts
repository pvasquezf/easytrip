import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs-page';
import { SitiosPage } from '../sitios/sitios';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'sitios',
        children: [
          {
            path: '',
            component: SitiosPage,
          },
          {
            path: 'sitio/:sitioId',
            loadChildren: () => import('../sitio-perfil/sitio-perfil.module').then(m => m.SitioPerfilModule)
          }
        ]
      },
      {
        path: 'iviaje', 
        loadChildren: './pages/iviaje/iviaje.module#IviajePageModule' 
      },
      {
        path: 'portalviaje', 
        loadChildren: './pages/portal-viaje/portal-viaje.module#PortalViajePageModule' 
      },
      {
        path: 'planificacion', 
        loadChildren: './planificacion/planificacion.module#PlanificacionPageModule'
      },
      {
        path: 'recomendar', 
        loadChildren: './recomendar/recomendar.module#RecomendarPageModule'
      },
      {
        path: 'speakers',
        children: [
          {
            path: '',
            loadChildren: () => import('../speaker-list/speaker-list.module').then(m => m.SpeakerListModule)
          },
          {
            path: 'session/:sessionId',
            loadChildren: () => import('../sitio-perfil/sitio-perfil.module').then(m => m.SitioPerfilModule)
          },
          {
            path: 'speaker-details/:speakerId',
            loadChildren: () => import('../speaker-detail/speaker-detail.module').then(m => m.SpeakerDetailModule)
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: () => import('../map/map.module').then(m => m.MapModule)
          }
        ]
      },
      {
        path: 'perfil',
        children: [
          {
            path: '',
            loadChildren: () => import('../perfil-usuario/perfil-usuario.module').then(m => m.PerfilUsuarioModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }

