import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import { Services } from '../../providers/services';
import { MenuController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {
  login: UserOptions = { username: '', password: '' };
  load: any;
  submitted = false;
  badCredentials = true;
  constructor(
    public menu: MenuController,
    public userData: UserData,
    public router: Router,
    public loadingCtrl: LoadingController,
    public services: Services
  ) { }


  async ngOnInit() {
    this.load = await this.loadingCtrl.create({
      spinner: 'lines',
      showBackdrop: true,
      message: "Espere por favor...",
      translucent: true,
      cssClass: 'ion-loading',
      duration: 0
    });
    await this.load.present();
    this.userData.isLoggedIn().then(m => {
      this.load.dismiss(this.loadingCtrl);
      if (m) {
        this.menu.enable(true);
        return this.router.navigateByUrl('/app/tabs/sitios');
      }
      this.menu.enable(false);
    });
  }

  async onLogin(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      this.load = await this.loadingCtrl.create({
        spinner: 'lines',
        showBackdrop: true,
        message: "Espere por favor...",
        translucent: true,
        cssClass: 'ion-loading',
        duration: 30000
      });
      await this.load.present();
      this.services.getLogin({ "email": this.login.username, "pass": this.login.password }).subscribe(json => {
        this.load.dismiss(this.loadingCtrl);
        if (json['codResultado'] != undefined) {
          this.badCredentials = false;
        } else {
          this.userData.login(this.login.username, json['id_usuario']);
          this.badCredentials = true;
          this.menu.enable(true);
          this.router.navigateByUrl('/app/tabs/sitios');
        }
      });
      //this.router.navigateByUrl('/app/tabs/sitios');
    }
  }

  onSignup() {
    this.router.navigateByUrl('/signup');
  }
}