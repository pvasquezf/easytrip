import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { PerfilUsuarioPage } from './perfil-usuario';
import { PerfilUsuarioPageRoutingModule } from './perfil-usuario-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    PerfilUsuarioPageRoutingModule
  ],
  declarations: [
    PerfilUsuarioPage,
  ]
})
export class PerfilUsuarioModule { }
