import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ViajeService } from 'src/app/services/viaje.service';
import {Observable} from 'rxjs';    


@Component({
  selector: 'app-update-viaje',
  templateUrl: './update-viaje.component.html',
  styleUrls: ['./update-viaje.component.scss'],
})
export class UpdateViajeComponent implements OnInit {

  id_viaje:number;
  id:number
  data:object = {};

  exist = false;
  productObj:object = {};
  nombreviaje = '';

  idviaje = null;
  idusuario = null;
  viajenombre = null;

  private headers1 = new Headers({ 'Content-Type': 'application/json'});

  constructor(private activatedRoute: ActivatedRoute,private router: Router, private route: ActivatedRoute, private http: HttpClient) { }
  viaje = [];
  products =[];
  idglob= '';
  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.idglob = id;

    this.route.params.subscribe(params => {
      this.id_viaje = +params['id_viaje'];
    });

  }

  fetchData = function(){
    this.http.get(`http://localhost:8100/`).subscribe(
      (res: Response) => {
        this.viaje = res.json();
      })
  }

  UpdateViaje() {
    
    let idnumviaje = +this.idglob;
    this.http.put('https://easytrip-253600.appspot.com/viaje/'+idnumviaje, {
      id_viaje: this.idviaje,
      nombre: this.nombreviaje,
      id_usuario: 3
    }).subscribe((response) => {
      console.log(response);

    })

  }



}
