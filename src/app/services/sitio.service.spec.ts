import { TestBed } from '@angular/core/testing';
import { SitioService } from './sitio.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Data } from '@angular/router';

describe('Feature: Servicio Para recomendar sitio', () => {

  let sitioService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    sitioService = new SitioService(httpClient);
  });

  afterEach(() => {
    sitioService = null;
    httpTestingController.verify();
  });
  describe('Escenario: Quiero llamar al servicio', () => {
    it('GIVEN necesito el servicio WHEN invoco servicio THEN servicio es creado exitosamente', () => {
      const service: SitioService = TestBed.get(SitioService);
      expect(service).toBeTruthy();
    });
  });

  describe('Escenario: Quiero consultar mi lista de paises en la base de datos', () => {
    it("GIVEN tengo paises en mi base de datos WHEN consulto el primer pais THEN id pais es '1' y pais es 'Guatemala'", function (done) {
      let esperado: boolean = true;
      let actual: boolean = false;
      sitioService.GetPaises().then(result => result.json()).then(json => {
        expect(json[0]['id_pais']).toEqual(1);
        expect(json[0]['pais']).toEqual("Guatemala");
        if (json[0]['pais'] === "Guatemala" && json[0]['id_pais'] == 1) {
          actual = true;
        }
        expect(esperado).toBe(actual);
        done();
      })
    });

    it("GIVEN tengo paises en mi base de datos WHEN consulto todos los datos THEN mi lista de paises no esta vacia", function (done) {
      sitioService.GetPaises().then(result => result.json()).then(json => {
        expect(json.length).toBeGreaterThan(0);
        done();
      })
    });
  });

  describe('Escenario: Quiero insertar un sitio nuevo a la base de datos', () => {
    it("GIVEN quiero recomendar un sitio turistico WHEN invoco mi servicio post THEN inserta un sitio a la base de datos", () => {
      const testData: Data = { Salida: true, Descripcion: "Se ha insertado en la tabla planificacion correctamente.", codResultado: 0 };
      sitioService.PostSitio("nombrepais", 4, 4, 4, 4, "nombreciudad", "imagen", "descripcion", 1).subscribe((response) => {
        expect(response).toEqual(testData)
      });
      const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/sitio/nuevo');
      expect(req.request.method).toEqual('POST');
      httpTestingController.verify();
    });

  });

});


