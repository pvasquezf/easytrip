import { TestBed } from '@angular/core/testing';

import { PlanificacionService } from './planificacion.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Data } from '@angular/router';

describe("Provider: Planificacion", () => {
  let planificacionService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    planificacionService = new PlanificacionService(httpClient);
  });

  afterEach(() => {
    planificacionService = null;
    httpTestingController.verify();
  });

  it("Get datos planificacion con id 1 de usuario", function (done) {
    let esperado: boolean = true;
    let actual: boolean = false;
    planificacionService.GetPlanificacion(1).then(result => result.json()).then(json => {
      expect(json[0]['id_planificacion']).toEqual(10);
      expect(json[0]['nombre']).toEqual("Segunda Planificacion");
      expect(json[0]['id_usuario']).toEqual(1);
      if (json[0]['id_planificacion'] == 10 && json[0]['nombre'] === "Segunda Planificacion" && json[0]['id_usuario'] == 1) {
        actual = true;
      }
      expect(esperado).toBe(actual);
      done();
    })


  });

  it("Insert planificacion en bd", () => {
    const testData: Data = { Salida: true, Descripcion: "Se ha insertado en la tabla planificacion correctamente.", codResultado: 0 };
    planificacionService.PostPlanificacion(1, "prueba").subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planificacion/nuevaplanificacion');
    expect(req.request.method).toEqual('POST');
    httpTestingController.verify();
  });

  it("Delete planificacion en bd", () => {
    const testData: Data = { Descripcion: "Se ha borrado en la tabla planificacion correctamente.", codResultado: 0 };
    planificacionService.DeletePlanificacion(1).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planificacion/borrar/1');
    expect(req.request.method).toEqual('DELETE');
    httpTestingController.verify();
  });

  it("Edit nombre planificacion", () => {
    const testData: Data = { Descripcion: "Se ha actualizado en la tabla planificacion correctamente.", codResultado: 0 };
    planificacionService.ActualizarPlanificacion(1, "prueba", 1).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planificacion/edit/1');
    expect(req.request.method).toEqual('PUT');
    httpTestingController.verify();
  });

  /*it("Get datos planificacion 1 con id 1 de usuario", function (done) {
    let esperado: boolean = true;
    let actual: boolean = false;
    planificacionService.GetPlanViaje(1).then(result => result.json()).then(json => {
      expect(json[0]['id_planificacion']).toEqual(1);
      expect(json[0]['nombre']).toEqual("primerrul");
      expect(json[0]['id_viaje']).toEqual(6);
      expect(json[0]['tiempo']).toEqual(0);
      expect(json[0]['presupuesto']).toEqual(0);
      if (json[0]['tiempo'] == 0 && json[0]['presupuesto'] == 0 && json[0]['id_planificacion'] == 1 && json[0]['nombre'] === "primerrul" && json[0]['id_viaje'] == 6) {
        actual = true;
      }
      expect(esperado).toBe(actual);
      done();
    })
  });*/

  it("Get viajes usuario 1", function (done) {
    let esperado: boolean = true;
    let actual: boolean = false;
    planificacionService.GetPlanViajeViajes(1).then(result => result.json()).then(json => {
      expect(json[0]['id_viaje']).toEqual(1);
      expect(json[0]['nombre']).toEqual("prueba");
      if (json[0]['nombre'] === "prueba" && json[0]['id_viaje'] == 1) {
        actual = true;
      }
      expect(esperado).toBe(actual);
      done();
    })
  });

  it("Get planificacion con id 1", function (done) {
    let esperado: boolean = true;
    let actual: boolean = false;
    planificacionService.GetSinglePlanificacion(9).then(result => result.json()).then(json => {
      expect(json[0]['id_planificacion']).toEqual(9);
      expect(json[0]['nombre']).toEqual("primerplanificacion");
      expect(json[0]['id_usuario']).toEqual(3);
      if (json[0]['id_planificacion'] == 9 && json[0]['nombre'] === "primerplanificacion" && json[0]['id_usuario'] == 3) {
        actual = true;
      }
      expect(esperado).toBe(actual);
      done();
    })
  });

  it("Insert viaje en planificacion en bd", () => {
    const testData: Data = { Salida: true, Descripcion: "Se ha insertado en la tabla VxP correctamente.", codResultado: 0 };
    planificacionService.PostPlanViaje(1, 6, null, null).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planviaje/nuevo');
    expect(req.request.method).toEqual('POST');
    httpTestingController.verify();
  });

  it("Delete viaje en planificacion en bd", () => {
    const testData: Data = { Descripcion: "Se ha borrado en la tabla VxP correctamente.", codResultado: 0 };
    planificacionService.DeletePlanViaje(1, 6).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planviaje/borrar/1/6');
    expect(req.request.method).toEqual('DELETE');
    httpTestingController.verify();
  });

  it("Edit fecha de inicio planificacion", () => {
    const testData: Data = { Descripcion: "Se ha actualizado en la tabla planificacion correctamente.", codResultado: 0 };
    planificacionService.ActualizarFechaInicio(1, '2019-03-19', 1).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planificacion/edit/fechainicio/1');
    expect(req.request.method).toEqual('PUT');
    httpTestingController.verify();
  });

  it("Edit fecha de fin planificacion", () => {
    const testData: Data = { Descripcion: "Se ha actualizado en la tabla planificacion correctamente.", codResultado: 0 };
    planificacionService.ActualizarFechaFin(1, '2019-03-19', 1).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/planificacion/edit/fechafin/1');
    expect(req.request.method).toEqual('PUT');
    httpTestingController.verify();
  });


});