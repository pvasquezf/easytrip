import { TestBed } from '@angular/core/testing';
import { ViajeportalService } from './viajeportal.service';
import { not } from '@angular/compiler/src/output/output_ast';
import { doesNotReject } from 'assert';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpBackend } from '@angular/common/http';
import { inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { Data } from '@angular/router';

describe("Service Viajes", () => {
  let viajeportalService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    viajeportalService = new ViajeportalService(httpClient);
  });

  afterEach(() => {
    viajeportalService = null;
    httpTestingController.verify();
  });

  it("Given que el acceso a los datos funciona \n When consulto los viajes de un usuario valido \n Then obtengo datos validos y no mensaje de error", function (done) {
    let esperado: boolean = true;
    let actual: boolean = false;
    viajeportalService.FetchViajes(1).then(result => result.json()).then(json => {
      expect(json[0]['id_viaje']).toEqual(1);
      expect(json[0]['nombre']).toEqual("prueba");
      expect(json[0]['id_usuario']).toEqual(1);
      if(json[0]['id_viaje'] == 1 && json[0]['nombre']==="prueba" && json[0]['id_usuario'] == 1)
      {
       actual = true;
      }
      expect(esperado).toBe(actual);
      done();
    })

    
  });

  it("Given que el acceso a los datos funciona \n When creo un viaje nuevo \n Then se crea en la base de datos", () => {
    const testData: Data = { Salida: true, Descripcion: "Se ha insertado en la tabla viaje correctamente", codResultado: 0 };
    viajeportalService.PostViajes(1, "prueba").subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/viaje');
    expect(req.request.method).toEqual('POST');
    httpTestingController.verify();
  });

  it("Given que el acceso a los datos funciona \n When solicito borrar un viaje \n Then se borra la informacion de tal viaje", () => {
    const testData: Data = { Descripcion: "Se ha borrado en la tabla viaje correctamente.", codResultado: 0 };
    viajeportalService.DeleteViajes(3).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/viaje/3');
    expect(req.request.method).toEqual('DELETE');
    httpTestingController.verify();
  });

  it("Given que el acceso a los datos funciona \n When solicito editar un viaje \n Then se edita la informacion de tal viaje", () => {
    const testData: Data = { Descripcion: "Se ha actualizado en la tabla viaje correctamente.", codResultado: 0 };
    viajeportalService.ActualizarViajes(1, "prueba", 3).subscribe((response) => {
      expect(response).toEqual(testData)
    });
    const req = httpTestingController.expectOne('https://easytrip-253600.appspot.com/viaje/3');
    expect(req.request.method).toEqual('PUT');
    httpTestingController.verify();
  });

  it("Given que consulto la api \n And existen los datos \n When consulto los viajes \n Then obtengo data ", () => {
    let viajes = viajeportalService.viajes;
    let errorm = '{"Descripcion":"No se han encontrado datos","codResultado":-1}';
    expect(viajes).not.toEqual(errorm);
  });
});