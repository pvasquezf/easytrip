import { TestBed } from '@angular/core/testing';
import { PortalViajexpaisService } from './portal-viajexpais.service';
import { not } from '@angular/compiler/src/output/output_ast';
import { doesNotReject } from 'assert';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpBackend } from '@angular/common/http';
import { inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { Data } from '@angular/router';

describe("Provider: PortalViajexpaisService", () => {
  let portalViajexpaisService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    portalViajexpaisService = new PortalViajexpaisService(httpClient);
  });


  afterEach(() => {
    portalViajexpaisService = null;
    httpTestingController.verify();
  });
/*
  it("should have a non empty array called products", () => {
    let viajesxpais = portalViajexpaisService.viajesxpais;

    expect(Array.isArray(viajesxpais)).toBeTruthy();
    expect(viajesxpais.length).toBeGreaterThan(0);
  });
*/
it("Given que el acceso a los datos funciona \n When consulto los viajes de un usuario valido \n Then obtengo datos validos y no mensaje de error", function (done) {
  let esperado: boolean = true;
  let actual: boolean = false;
  portalViajexpaisService.FetchViajes(1).then(result => result.json()).then(json => {
    expect(json[0]['id_viaje']).toEqual(1);
    expect(json[0]['nombre']).toEqual("prueba");
    expect(json[0]['id_usuario']).toEqual(1);
    if(json[0]['id_viaje'] == 1 && json[0]['nombre']==="prueba" && json[0]['id_usuario'] == 1)
    {
     actual = true;
    }
    expect(esperado).toBe(actual);
    done();
  })

  
});
});