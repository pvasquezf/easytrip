import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ViajeService {

  constructor(private http: HttpClient) { }

  getViajexUser(id)
  {
    return this.http.get(`https://easytrip-253600.appspot.com/viajes/${id}`)
  }

  getPaisxViaje(id,idviaje)
  {
    return this.http.get(`https://easytrip-253600.appspot.com/${id}/viaje/${idviaje}/pais`)
  }
  
  deleteTask(id: string) {
    const path = `https://easytrip-253600.appspot.com/viaje/${id}`;
    return this.http.delete(path);
  }

  getPaises()
  {
    return this.http.get(`https://easytrip-253600.appspot.com/pais/`)
  }
}
