import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class PortalViajexpaisService {

  public viajesxpais: any[];
  public viajes: any[];
  public id;


  constructor(private http: HttpClient) { 

    /*
    this.viajesxpais = [
      { 
        id_pais: 1, 
        pais: "Prueba1-viajexpais" 
      },
      {
        id_pais: 2,
        pais: "Prueba2-viajexpais"
      },
      { 
        id_pais: 3,
        pais: "Prueba3-viajexpais"
      }
    ];
    */

  }

  FetchViajes(id)
  {
    return fetch(`https://easytrip-253600.appspot.com/viajes/${id}`);
    //https://easytrip-253600.appspot.com/91/viaje/6/pais 
  }

  getViajexUser1(id)
  {
    return this.http.get(`https://easytrip-253600.appspot.com/viajes/${id}`)
  }

  getPaisxViaje1(id,idviaje)
  {
    return this.http.get(`https://easytrip-253600.appspot.com/${id}/viaje/${idviaje}/pais`)
  }
  
  deleteTask1(id: string) {
    const path = `https://easytrip-253600.appspot.com/viaje/${id}`;
    return this.http.delete(path);
  }

  getPaises1()
  {
    return this.http.get(`https://easytrip-253600.appspot.com/pais/`)
  }



}
