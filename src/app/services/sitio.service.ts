import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SitioService {
  public listapaises: any[];
  constructor(private http: HttpClient) { }
  
  GetPaises()
  {
    return fetch(`https://easytrip-253600.appspot.com/pais/`);
  }

  PostSitio(inputnombre, inputcalificacion, inputprecio, inputlatitud, inputlongitud, inputciudad, inputimagen, inputdescripcion, inputid_pais)
  {
    return this.http.post('https://easytrip-253600.appspot.com/sitio/nuevo', {
      nombre: inputnombre, 
      calificacion: inputcalificacion, 
      precio: inputprecio, 
      latitud: inputlatitud, 
      longitud: inputlongitud, 
      ciudad: inputciudad, 
      imagen: inputimagen, 
      descripcion: inputdescripcion, 
      id_pais: inputid_pais
    });
  }

}
