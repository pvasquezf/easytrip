import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckTutorial } from './providers/check-tutorial.service';
import { UpdateViajeComponent } from './../app/update-viaje/update-viaje.component';
import { PerfilUsuarioPageRoutingModule } from './pages/perfil-usuario/perfil-usuario-routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'perfil',
    loadChildren: () => import('./pages/perfil-usuario/perfil-usuario.module').then(m => m.PerfilUsuarioModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./pages/support/support.module').then(m => m.SupportModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignUpModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialModule),
    canLoad: [CheckTutorial]
  },
  {
    path: 'iviaje', 
    loadChildren: './pages/iviaje/iviaje.module#IviajePageModule' 
  },
  { path: 'viajes/:id', loadChildren: './pages/viajes/viajes.module#ViajesPageModule' },
  { path: 'viajes/:id/:idviaje/paises', loadChildren: './pages/paisxviaje/paisxviaje.module#PaisxviajePageModule' },
  { path: 'portal-viajexpais', loadChildren: './portal-viajexpais/portal-viajexpais.module#PortalViajexpaisPageModule' },
  { path: 'portal-viaje', loadChildren: './pages/portal-viaje/portal-viaje.module#PortalViajePageModule' },
  { path: 'planificacion', loadChildren: './planificacion/planificacion.module#PlanificacionPageModule' },
  { path: 'planviajes/:idplanificacion/:idusuario', loadChildren: './planviajes/planviajes.module#PlanviajesPageModule' },  { path: 'recomendar', loadChildren: './recomendar/recomendar.module#RecomendarPageModule' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)], 
  exports: [RouterModule]
})
export class AppRoutingModule {}