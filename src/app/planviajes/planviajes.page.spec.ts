import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanviajesPage } from './planviajes.page';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/internal/observable/of';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('PlanviajesPage', () => {
  let component: PlanviajesPage;
  let fixture: ComponentFixture<PlanviajesPage>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanviajesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ActivatedRoute, 
          useValue: { 
            snapshot: { 
              paramMap: {
                get:(idusuario:number,idplanificacion:number)=>{idusuario:91;idplanificacion:1}
              }
            }
          },
        },
      ]
        
        })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanviajesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('Debe Crear pagina', () => {
    expect(component).toBeTruthy();
  });
});
