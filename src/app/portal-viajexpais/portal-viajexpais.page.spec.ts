import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";
import { PortalViajexpaisService} from "../services/portal-viajexpais.service"
import { PortalViajexpaisPage } from './portal-viajexpais.page';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Data } from '@angular/router';
import { AngularDelegate } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';


describe('PortalViajexpaisPage', () => {
  let component: PortalViajexpaisPage;
  let fixture: ComponentFixture<PortalViajexpaisPage>;
  let de: DebugElement;
  let el: HTMLElement;

  let portalViajexpaisService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalViajexpaisPage ],
      providers: [PortalViajexpaisService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule, IonicStorageModule.forRoot()]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalViajexpaisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    portalViajexpaisService = new PortalViajexpaisService(httpClient);
  });

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
  });

  it("Given que tengo el portal definido \n When ingreso al portal \n Then la pagina es creada", () => {
    expect(fixture).toBeTruthy();
    expect(component).toBeTruthy();
  });

  it("Given que tengo uno o mas viajes x pais creados \n When abro el portal e ingreso  \n Then el nombre de los paises de mis viajes",() => {
    let esperado: boolean = true;
    let actual: boolean = true;
      const testData: Data = {Descripcion: "No se han encontrado datos", codResultado: -1 };

      portalViajexpaisService.FetchViajes(portalViajexpaisService.id).then(result => result.json()).then(json => {
        actual = false;
        if(JSON.stringify(json).toLowerCase() === JSON.stringify(testData).toLowerCase())
        {
          actual = true;
        }
        else
        {
          let primerviaje = json[0];
          fixture.detectChanges();
          de = fixture.debugElement.query(By.css("ion-list ion-item"));
          el = de.nativeElement;
          expect(el.textContent).toContain(primerviaje.nombre);
          actual = true;
        }
      })
      expect(actual).toBe(esperado);
  });
});
