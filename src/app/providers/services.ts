import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class Services {
    data: any;
    url = 'https://easytrip-253600.appspot.com/';
    constructor(public http: HttpClient) { }

    getData() {
        return this.http.get(this.url + 'sitio');
    }

    getProfileData(idSitio) {
        return this.http.get(this.url + 'sitio/' + idSitio);
    }

    getSites() {
        return this.http.get(this.url + 'sitio');
    }

    getFavsUser(idUser) {
        return this.http.get(this.url + 'favorito/' + idUser);
    }

    addfav(body) {
        return this.http.post(this.url + 'favorito', body);
    }

    removefav(body) {
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
            body,
        };
        return this.http.delete(this.url + 'favorito', options);
    }

    getCountries() {
        return this.http.get(this.url + 'pais');
    }

    getCommentary(sitioId) {
        return this.http.get(this.url + 'comentario/' + sitioId);
    }

    addComment(datos) {
        return this.http.post(this.url + 'comentario', datos, {});
    }

    getLogin(data) {
        return this.http.post(this.url + 'user/login', data, {});
    }

    postUser(data) {
        return this.http.post(this.url + 'user/new', data, {});
    }

    userData(usuario: number) {
        return this.http.get(this.url + 'user/id/' + usuario);
    }
}