import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Services } from '../providers/services';

@Injectable({
  providedIn: 'root'
})
export class UserData {
  _favorites: any[];
  _sitios: any[];
  _idUsuario: number;
  HAS_LOGGED_IN = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';

  constructor(
    public events: Events,
    public storage: Storage,
    public services: Services
  ) { }

  hasFavorite(sessionName: number): boolean {
    if (this._favorites != undefined) {
      return (this._favorites.includes(sessionName));
    }else{
      this._favorites = [];
      return false;
    }
  }

  addFavorite(sessionName: number): void {
    this._favorites.push(sessionName);
  }

  setFavorite(favs: []): void {
    this._favorites = favs;
  }

  getFavorite() {
    return this._favorites;
  }

  setSites(sitios: []): void {
    this._sitios = sitios;
  }

  getSites() {
    return this._sitios;
  }

  removeFavorite(sessionName: number): void {
    const index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  }

  login(username: string, idUsuario: number): Promise<any> {
    return this.storage.set(this.HAS_LOGGED_IN, true).then(() => {
      this._idUsuario = idUsuario;
      this.setUsername(username);
      this.setUserId(idUsuario);
      return this.events.publish('user:login');
    });
  }

  signup(username: string): Promise<any> {
    return this.storage.set(this.HAS_LOGGED_IN, true).then(() => {
      this.setUsername(username);
      return this.events.publish('user:signup');
    });
  }

  logout(): Promise<any> {
    return this.storage.remove(this.HAS_LOGGED_IN).then(() => {
      this._idUsuario = undefined;
      return this.storage.remove('username');
    }).then(() => {
      this.events.publish('user:logout');
    });
  }

  setUsername(username: string): Promise<any> {
    return this.storage.set('username', username);
  }

  setUserId(userid: number): Promise<any> {
    this._idUsuario = userid;
    return this.storage.set('idUser', userid);
  }

  getUsername(): Promise<string> {
    return this.storage.get('username').then((value) => {
      return value;
    });
  }

  getUserId(): Promise<number> {
    return this.storage.get('idUser').then((value) => {
      return value;
    });
  }

  isLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  }

  checkHasSeenTutorial(): Promise<string> {
    return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
      return value;
    });
  }
}
