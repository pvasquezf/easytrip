import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class ServicesTest {
    url = 'https://easytrip-253600.appspot.com/';
    getLogin(user: string, pass: string) {
        return fetch(this.url + 'user/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ "email": user, "pass": pass })
        });
    }

    postUser(nombre: string, apellido: string,email: string, pass: string, edad: number){
        return fetch(this.url + 'user/new', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"nombre":nombre,"apellido":apellido, "email": email, "pass": pass,"edad": edad })
        });
    }

    userData(usuario: number){
        return fetch(this.url + 'user/id/'+usuario);
    }
}