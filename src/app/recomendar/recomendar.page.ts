import { Component, OnInit } from '@angular/core';
import { SitioService } from '../services/sitio.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-recomendar',
  templateUrl: './recomendar.page.html',
  styleUrls: ['./recomendar.page.scss'],
})
export class RecomendarPage implements OnInit {
  inputnombre = null;
  inputcalificacion=null;
  inputprecio = null;
  inputlatitud = null;
  inputlongitud = null;
  inputciudad = null;
  inputimagen = null; 
  inputdescripcion = null;
  paisescogido=null;
  constructor(private sitioService: SitioService, private http: HttpClient) { }

  ngOnInit() {
    this.cargar();
  }

  cargar()
  {

    this.inputnombre = null;
    this.inputcalificacion=null;
    this.inputprecio = null;
    this.inputlatitud = null;
    this.inputlongitud = null;
    this.inputciudad = null;
    this.inputimagen = null; 
    this.inputdescripcion = null;
    this.paisescogido=null;
    this.sitioService.GetPaises().then(result => result.json()).then(json => { 
      this.sitioService.listapaises = json;
    })

  }

  sendPostRequest()
  {
    if(this.paisescogido != null)
    {
      this.sitioService.PostSitio(this.inputnombre, this.inputcalificacion, this.inputprecio, this.inputlatitud, this.inputlongitud, this.inputciudad, this.inputimagen, this.inputdescripcion, this.paisescogido).subscribe((response) => {
        console.log("POST RESULT",response);
        this.cargar();
      });
    }
     
  }

}
