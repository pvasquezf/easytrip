export function Gestionar(id_viaje:number, id_pais:number,id_sitio:number,cantidad$:number,tiempo:number){
    let url = 'https://easytrip-253600.appspot.com/';
    if(id_pais!=-1){
        return fetch(url + 'update/presupuesto/pais', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: 
            JSON.stringify({ 
                "id_viaje": id_viaje,
                "id_pais": id_pais, 
                "tiempo":tiempo,
                "presupuesto":cantidad$
            })
        });
    }else {
       return fetch(url + 'update/presupuesto/sitio', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: 
            JSON.stringify({ 
                "id_sitio": id_sitio,
                "id_pais": id_pais, 
                "tiempo":tiempo,
                "presupuesto":cantidad$
            })
        });
    }
}