# EasyTrip

Aplicación móvil enfocada a la planificación de viajes

# Compilar aplicacion
1. git clone https://gitlab.com/pvasquezf/easytrip.git
2. Abrir una terminal en la carpeta del proyecto
3. ejecutar el comando npm install
4. Este comando descargara todas las dependencias que se encuentran listadas en el archivo package.json ya que sin estas no funcionara la app
5. Esto seria todo, para correr en el navegador ejecutar en la terminal **ionic serve** y para android **ionic cordova run android**